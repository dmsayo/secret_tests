#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#define SECRET "test_rmult"
#define SECRET_SIZE 8192

char msg[SECRET_SIZE + 2000];

int main(int argc, char *argv[])
{
   int i;
   int fd;
   char buf[SECRET_SIZE];
   int end;
   pid_t pid;

   for (i = 0; i < 10000; i++)
   {
      msg[i] = 'k';
   }

   if ((fd = open("/dev/Secret", O_WRONLY)) < 0)
   {
      perror("/dev/Secret (open WRONLY)");
      exit(EXIT_FAILURE);
   }

   if (write(fd, msg, 10000) < 0)
   {
      perror("/dev/Secret (write)");
   }

   if (close(fd) < 0)
   {
      perror("/dev/Secret (close)");
      exit(EXIT_FAILURE);
   }

   if ((fd = open("/dev/Secret", O_RDONLY)) < 0)
   {
      perror("/dev/Secret (open RDONLY)");
      exit(EXIT_FAILURE);
   }
  
   end = read(fd, buf, 8193);
   buf[end] = '\0';
   printf("%s", buf);
   
   if (close(fd) < 0)
   {
      perror("/dev/Secret (close)");
      exit(EXIT_FAILURE);
   }
   
   return 0;
}
