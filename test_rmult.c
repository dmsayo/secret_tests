#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#define SECRET "test_rmult"
#define SECRET_SIZE 8192

char msg[SECRET_SIZE] = "test_rmult test_rmult test_rmult";

int main(int argc, char *argv[])
{
   int fd;
   char buf[SECRET_SIZE];
   int end;
   pid_t pid;

   if ((fd = open("/dev/Secret", O_WRONLY)) < 0)
   {
      perror("/dev/Secret (open WRONLY)");
      exit(EXIT_FAILURE);
   }

   if (write(fd, msg, strlen(msg)) < 0)
   {
      perror("/dev/Secret (write)");
      exit(EXIT_FAILURE);
   }

   if (close(fd) < 0)
   {
      perror("/dev/Secret (close)");
      exit(EXIT_FAILURE);
   }

   if ((fd = open("/dev/Secret", O_RDONLY)) < 0)
   {
      perror("/dev/Secret (open RDONLY)");
      exit(EXIT_FAILURE);
   }
  
   end = read(fd, buf, 5);
   buf[end] = '\0';
   printf("%s\n", buf);
   
   end = read(fd, buf, 3);
   buf[end] = '\0';
   printf("%s\n", buf);
   
   end = read(fd, buf, 8);
   buf[end] = '\0';
   printf("%s\n", buf);
   
   end = read(fd, buf, 1);
   buf[end] = '\0';
   printf("%s\n", buf);
   
   end = read(fd, buf, 90); /* Try to read past secret */
   buf[end] = '\0';
   printf("%s\n", buf);
   
   if (close(fd) < 0)
   {
      perror("/dev/Secret (close)");
      exit(EXIT_FAILURE);
   }
   
   return 0;
}
