#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#define SECRET "test_wmult test_wmult test_wmult\n"
#define SECRET_SIZE 34

char msg[SECRET_SIZE] = SECRET;

int main(int argc, char *argv[])
{
   int i;
   int fd;
   char buf[SECRET_SIZE + 1];
   int end;
   pid_t pid;

   if ((fd = open("/dev/Secret", O_WRONLY)) < 0)
   {
      perror("/dev/Secret (open WRONLY)");
      exit(EXIT_FAILURE);
   }

   if (write(fd, msg, 7) < 0)
   {
      perror("/dev/Secret (write)");
      exit(EXIT_FAILURE);
   }
   
   if (write(fd, msg+7, 13) < 0)
   {
      perror("/dev/Secret (write)");
      exit(EXIT_FAILURE);
   }
   
   if (write(fd, msg+20, 5) < 0)
   {
      perror("/dev/Secret (write)");
      exit(EXIT_FAILURE);
   }
   
   if (write(fd, msg+25, 9) < 0)
   {
      perror("/dev/Secret (write)");
      exit(EXIT_FAILURE);
   }

   if (close(fd) < 0)
   {
      perror("/dev/Secret (close)");
      exit(EXIT_FAILURE);
   }

   if ((fd = open("/dev/Secret", O_RDONLY)) < 0)
   {
      perror("/dev/Secret (open RDONLY)");
      exit(EXIT_FAILURE);
   }
  
   end = read(fd, msg, SECRET_SIZE);
   msg[end] = '\0';
   printf("%s", msg);
   
   if (close(fd) < 0)
   {
      perror("/dev/Secret (close)");
      exit(EXIT_FAILURE);
   }
   
   return 0;
}
