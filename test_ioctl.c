#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#define SECRET "test_ioctl\n"

int main(int argc, char *argv[])
{
   int fd;
   int res;
   int uid;

   fd = open("/dev/Secret", O_WRONLY);
   printf("Opening... fd=%d\n", fd);
   res = write(fd, SECRET, strlen(SECRET));
   printf("Writing... res=%d\n", res);
   /* try grant */
   if (argc > 1 && 0 != (uid=atoi(argv[1])))
   {
      if (res = ioctl(fd, SSGRANT, &uid))
      {
         perror("ioctl");
      }
      printf("Trying to change owner to %d...res=%d\n", uid, res);
   }
   res = close(fd);

   return 0;
}
