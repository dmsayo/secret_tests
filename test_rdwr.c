#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <fcntl.h>

int main(int argc, char *argv[])
{
   int fd;

   if ((fd = open("/dev/Secret", O_RDWR)) < 0)
   {
      perror("/dev/Secret (open)");
      exit(EXIT_FAILURE);
   }

   return 0;
}
